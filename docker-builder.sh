#!/usr/bin/env bash

### Docker Builder

# MIT License

# Copyright (c) 2019 Harley A.W. Lorenzo

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# The name of the project
NAME="peers-of-trust"

# Packages the docker image will automatically install during build
PACKAGES="build-essential gdb pkg-config cmake libsodium-dev"

# Additional docker build commands to run
BUILD_COMMANDS=""

# Any commands that should automatically run every time the image is ran
COMMANDS=""

# Options to pass to docker run
DOCKER_OPTS=""

show_usage ()
{
    >&2 printf "Usage: $0 [OPTIONS...]\n"
    >&2 printf "\nOptions\n"
    >&2 printf "\t-c OPTION\tAdditionally run these commands\n"
    >&2 printf "\t-d OPTION\tPass OPTION as an argument to docker run\n"
    >&2 printf "\t-i\t\tDo not run in interactive mode\n"
    >&2 printf "\t-n OPTION\tUse OPTION as project name\n"
    >&2 printf "\t-p OPTION\tUse OPTION as project directory\n"
    >&2 printf "\t-r\t\tRebuild the docker image\n"
    exit 1
}

# Builds or reebuilds the docker image
build_docker ()
{
    # The template docker file that is used to build
    DOCKER_FILE="FROM ubuntu:latest
    run mkdir /$NAME/
    WORKDIR /$NAME/
    RUN apt update
    RUN apt full-upgrade -y
    RUN apt install -y $PACKAGES
    $BUILD_COMMANDS"

    1>/dev/null 2>/dev/null docker rmi $NAME
    echo "$DOCKER_FILE" > $PROJECT_DIR/Dockerfile.$NAME
    docker build --tag=$NAME -f $PROJECT_DIR/Dockerfile.$NAME \
        $PROJECT_DIR
    rm $PROJECT_DIR/Dockerfile.$NAME
}

PROJECT_DIR="$( cd "$( dirname "$0" )" && pwd )"

REBUILD=false
OPT_C=false
OPT_D=false
OPT_P=false
OPT_N=false
INTERACTIVE="-it"

for ARG in "$@"   
do
    # If the previous argument was one with an option, grab it now
    if [[ "$OPT_C" == true ]]; then
        COMMANDS="$COMMANDS $ARG" 
        OPT_C=false 
        continue
    elif [[ "$OPT_D" == true ]]; then
        DOCKER_OPTS="$DOCKER_OPTS $ARG"
        OPT_D=false
        continue
    elif [[ "$OPT_P" == true ]]; then
        PROJECT_DIR=$(cd $ARG; pwd) 
        OPT_P=false 
        continue
    elif [[ "$OPT_N" == true ]]; then
        NAME="$ARG" 
        OPT_N=false 
        continue
    # Regular argument parsing
    elif [[ $ARG == "show_usage" ]]; then
        show_usage
    elif [[ $ARG == "-h" ]]; then
        show_usage
    elif [[ $ARG == "--help" ]]; then
        show_usage
    elif [[ $ARG == "-c" ]]; then
        OPT_C=true
    elif [[ $ARG == "-d" ]]; then
        OPT_D=true
    elif [[ $ARG == "-i" ]]; then
        INTERACTIVE=""
    elif [[ $ARG == "-n" ]]; then
        OPT_N=true
    elif [[ $ARG == "-p" ]]; then
        OPT_P=true
    elif [[ $ARG == "-r" ]]; then
        REBUILD=true
    else
        >&2 echo "Unknown argument: $ARG"
    fi
done

if [[ -z $NAME ]]; then
    >&2 echo "Please input a project name inside the script"
    exit 1
fi

# Check if the image exists, building it automatically if it doesn't
if [[ -z "$(docker images | egrep -v "^REPOSITORY\s*TAG.*" | \
        egrep "^$NAME")" ]]; then
    >&2 echo "$NAME docker image not found, building..."
    build_docker
elif [[ "$REBUILD" == true ]]; then
    build_docker
fi

# If commands do exist, append a && to ensure bash is ran 
if [[ -n $COMMANDS ]]; then
    COMMANDS="$COMMANDS &&"
fi

docker run $DOCKER_OPTS $INTERACTIVE --rm -v ${PROJECT_DIR}:/$NAME/ \
    $NAME:latest bash -c "$COMMANDS bash"
