/* Peers of Trust
 * Copyright (C) 2019  Harley A.W. Lorenzo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can contact Harley A.W. Lorenzo via the email hl1998@protonmail.com
 * Harley A.W. Lorenzo's GPG key fingerprint is:
 * 72BC 96FB D08A CFA8 BBD8 D1D6 F6EF 2390 4645 BA53 */

#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "main.h"

int main(int argc, char **argv)
{
    int r;
    if ((r = key_test()))
    {
        return r;
    }
    return 0;
}

int key_test(void)
{
    printf("Unit KEY_TEST: testing...\n");

    struct pot_pubkey * pubkey = (struct pot_pubkey *)
        malloc(sizeof(struct pot_pubkey));
    struct pot_privkey * privkey = (struct pot_privkey *)
        malloc(sizeof(struct pot_privkey));
    unsigned int message_len = 1048576;
    uint8_t * message = (uint8_t *) malloc(message_len);
    uint8_t * signature = NULL;
    uint8_t * hash = NULL;
    if (pubkey == NULL || privkey == NULL || message == NULL)
    {
        perror("Unit KEY_TEST: failed");
        goto fail;
    }
    /* i = key algo id to test, j = fingerprint algo to test */
    for (unsigned int i = 1; i < pot_key_algo_list_len; i++)
    {
        for (unsigned int j = 1; j < pot_fingerprint_algo_list_len; j++)
        {
            signature = (uint8_t *) malloc(pot_key_algo_list[i].signature_len);
            hash = (uint8_t *) malloc(pot_fingerprint_algo_list[j].len);
            if (signature == NULL || hash == NULL)
            {
                goto fail;
            }

            if (pot_key_gen(pubkey, privkey, &pot_key_algo_list[i],
                &pot_fingerprint_algo_list[j]))
            {
                goto fail;
            }

            if (privkey->key_algo.sign(signature, message, message_len,
                privkey->data))
            {
                fprintf(stderr, "Unit KEY_TEST: failed, reason: "
                    "failure signing with key_algo %s\n", privkey->key_algo.str);
                goto fail;
            }
            if (pubkey->key_algo.verify(signature, message, message_len,
                pubkey->data))
            {
                fprintf(stderr, "Unit KEY_TEST: failed, reason: "
                    "failure verifying signature with key_algo %s\n",
                    privkey->key_algo.str);
                goto fail;
            }

            if (pubkey->fingerprint.fingerprint_algo.hash(hash, pubkey->data,
                pubkey->key_algo.public_len))
            {
                fprintf(stderr, "Unit KEY_TEST: failed, reason: "
                    "failure hashing public key with algo %s\n",
                    pubkey->fingerprint.fingerprint_algo.str);
                goto fail;
            }
            if ((memcmp(hash, pubkey->fingerprint.data,
                pubkey->fingerprint.fingerprint_algo.len)) ||
                memcmp(hash, privkey->pubkey_fingerprint.data,
                privkey->pubkey_fingerprint.fingerprint_algo.len))
            {
                fprintf(stderr, "Unit KEY_TEST: failed, reason: "
                    "failure verifying public key hash with algo %s\n",
                    pubkey->fingerprint.fingerprint_algo.str);
                goto fail;
            }

            if (privkey->fingerprint.fingerprint_algo.hash(hash, privkey->data,
                privkey->key_algo.private_len))
            {
                fprintf(stderr, "Unit KEY_TEST: failed, reason: "
                    "failure hashing private key with algo %s\n",
                    privkey->fingerprint.fingerprint_algo.str);
                goto fail;
            }
            if ((memcmp(hash, privkey->fingerprint.data,
                privkey->fingerprint.fingerprint_algo.len)) ||
                memcmp(hash, pubkey->privkey_fingerprint.data,
                pubkey->privkey_fingerprint.fingerprint_algo.len))
            {
                fprintf(stderr, "Unit KEY_TEST: failed, reason: "
                    "failure verifying private key hash with algo %s\n",
                    privkey->fingerprint.fingerprint_algo.str);
                goto fail;
            }

            free(signature);
            free(hash);
            pot_key_del(pubkey, privkey);
        }
    }

    free(pubkey);
    free(privkey);
    free(message);

    printf("Unit KEY_TEST: tested successfully\n");
    return 0;
fail:
    pot_key_del(pubkey, privkey);
    free(pubkey);
    free(privkey);
    free(message);
    free(signature);
    free(hash);

    return -1;
}
