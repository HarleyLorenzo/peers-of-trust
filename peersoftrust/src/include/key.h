/* Peers of Trust
 * Copyright (C) 2019  Harley A.W. Lorenzo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can contact Harley A.W. Lorenzo via the email hl1998@protonmail.com
 * Harley A.W. Lorenzo's GPG key fingerprint is:
 * 72BC 96FB D08A CFA8 BBD8 D1D6 F6EF 2390 4645 BA53 */

#ifndef PEERS_OF_TRUST_KEY_H_
#define PEERS_OF_TRUST_KEY_H_

#include <stdint.h>
#include <pthread.h>
#include <sys/queue.h>

#include <sodium.h>

#include "ed25519.h"
#include "sha512.h"

typedef int_least8_t fingerprint_id;

struct pot_fingerprint_algo
{
    /* The internal and fast short representation of the algorithm
     * NOTE: We use -1 in id for an undefined algorithm */
    fingerprint_id id;
    /* The length of the fingerprint */
    int_fast16_t len;
    /* The "universal" identifier string we use for an algorithm in
     * any extra-process communication or handshake */
    char * str;
    /* The function that provides the hash */
    int (*hash) (uint8_t * hash, const uint8_t * message,
        const uint_fast32_t len);
};

#define pot_fingerprint_algo_list_len 2
const struct pot_fingerprint_algo
    pot_fingerprint_algo_list[pot_fingerprint_algo_list_len] =
{
    {
        .id = 0,
        .len = 0,
        .str = NULL,
        .hash = NULL,
    },
    /* SHA512 */
    {
        .id = 1,
        .len = crypto_hash_sha512_BYTES,
        .str = "SHA512",
        .hash = pot_sha512,
    },
};

typedef int_least8_t key_id;

struct pot_key_algo
{
    /* The internal and fast short representation of the algorithm
     * NOTE: We use -1 in id for an undefined algorithm */
    key_id id;
    /* How long the key itself is, with separate private and public
     * lengths for flexibility in algorithm */
    int_least16_t private_len;
    int_least16_t public_len;
    /* The length of the signature of the private key */
    int_least16_t signature_len;
    /* The "universal" identifier string we use for an algorithm in
     * any extra-process communication or handshake */
    char * str;
    int (*keygen) (uint8_t * pubkey, uint8_t * privkey);
    int (*sign) (uint8_t * signature, const uint8_t * message,
        const uint_fast32_t len, const uint8_t * privkey);
    int (*verify) (const uint8_t * signature, const uint8_t * message,
        const uint_fast32_t len, const uint8_t * pubkey);
};

#define pot_key_algo_list_len 2
const struct pot_key_algo pot_key_algo_list[pot_key_algo_list_len] =
{
    /* Undefined */
    {
        .id = 0,
        .private_len = 0,
        .public_len = 0,
        .signature_len = 0,
        .str = NULL,
        .keygen = NULL,
        .sign = NULL,
    },
    /* Ed25519 */
    {
        .id = 1,
        .private_len = crypto_sign_ed25519_SECRETKEYBYTES,
        .public_len = crypto_sign_ed25519_PUBLICKEYBYTES,
        .signature_len = crypto_sign_ed25519_BYTES,
        .str = "Ed25519",
        .keygen = pot_ed25519_keygen,
        .sign = pot_ed25519_sign,
        .verify = pot_ed25519_verify,
    },
};

struct pot_fingerprint
{
    struct pot_fingerprint_algo fingerprint_algo;
    uint8_t * data;
};

struct pot_sig
{
    uint8_t * data;
    struct pot_key_algo key_algo;
    struct pot_fingerprint fingerprint;
};

SLIST_HEAD(pot_sig_list_head, pot_sig_list);

struct pot_sig_list
{
    struct pot_sig sig;
    SLIST_ENTRY(pot_sig_list) entries;
};

struct pot_pubkey
{
    uint8_t * data;
    uint8_t * self_sig_data;
    struct pot_key_algo key_algo;
    struct pot_fingerprint fingerprint;
    struct pot_fingerprint privkey_fingerprint;
    struct pot_sig_list_head * signatures;
};

LIST_HEAD(pot_pubkey_list_real_head, pot_pubkey_list);

struct pot_pubkey_list
{
    struct pot_pubkey key;
    LIST_ENTRY(pot_pubkey_list) entries;
};

struct pot_pubkey_list_head
{
    struct pot_pubkey_list_real_head real_head;
    pthread_mutex_t lock;
};

struct pot_privkey
{
    uint8_t * data;
    struct pot_fingerprint fingerprint;
    struct pot_fingerprint pubkey_fingerprint;
    struct pot_key_algo key_algo;
};

int pot_key_gen(struct pot_pubkey * pubkey, struct pot_privkey * privkey,
    const struct pot_key_algo * key_algo,
    const struct pot_fingerprint_algo * fingerprint_algo);

int pot_key_init(struct pot_pubkey * pubkey, struct pot_privkey * privkey,
    const struct pot_key_algo * key_algo,
    const struct pot_fingerprint_algo * fingerprint_algo);

int pot_key_del(struct pot_pubkey * pubkey, struct pot_privkey * privkey);

#endif  //PEERS_OF_TRUST_KEY_H_
