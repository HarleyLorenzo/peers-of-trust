/* Peers of Trust
 * Copyright (C) 2019  Harley A.W. Lorenzo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can contact Harley A.W. Lorenzo via the email hl1998@protonmail.com
 * Harley A.W. Lorenzo's GPG key fingerprint is:
 * 72BC 96FB D08A CFA8 BBD8 D1D6 F6EF 2390 4645 BA53 */

#include <sodium.h>

#include "sha512.h"

int pot_sha512(uint8_t * hash, const uint8_t * message, const uint_fast32_t len)
{
    if (hash == NULL || message == NULL)
    {
        return -1;
    }
    if (crypto_hash_sha512(hash, message, len) != 0)
    {
        return -1;
    }
    return 0;
}
