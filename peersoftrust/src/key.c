/* Peers of Trust
 * Copyright (C) 2019  Harley A.W. Lorenzo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You can contact Harley A.W. Lorenzo via the email hl1998@protonmail.com
 * Harley A.W. Lorenzo's GPG key fingerprint is:
 * 72BC 96FB D08A CFA8 BBD8 D1D6 F6EF 2390 4645 BA53 */

#include <stdlib.h>
#include <string.h>

#include "ed25519.h"
#include "key.h"
#include "sha512.h"

int pot_key_gen(struct pot_pubkey * pubkey, struct pot_privkey * privkey,
    const struct pot_key_algo * key_algo,
    const struct pot_fingerprint_algo * fingerprint_algo)
{
    /* We need both a pubkey and a privkey struct to create a new keypair
     * as well as the key and fingerprint algos */
    if (pubkey == NULL || privkey == NULL || key_algo == NULL ||
        fingerprint_algo == NULL || key_algo->id == 0 ||
        fingerprint_algo->id == 0)
    {
        return -1;
    }
    /* Initialize keys, returning -1 on a failure */
    if (pot_key_init(pubkey, privkey, key_algo, fingerprint_algo))
    {
        return -1;
    }
    /* Generate the keys */
    if (key_algo->keygen(pubkey->data, privkey->data))
    {
        return -1;
    }

    /* Create a fingerprint buffer and assign fingerprints to both keys */
    uint8_t * fingerprint = malloc(fingerprint_algo->len);
    if (fingerprint == NULL)
    {
        return -1;
    }
    if (fingerprint_algo->hash(fingerprint, pubkey->data,
        key_algo->public_len))
    {
        free(fingerprint);
        return -1;
    }
    memcpy(pubkey->fingerprint.data, fingerprint, fingerprint_algo->len);
    memcpy(privkey->pubkey_fingerprint.data, fingerprint,
        fingerprint_algo->len);

    if (fingerprint_algo->hash(fingerprint, privkey->data,
        key_algo->private_len))
    {
        free(fingerprint);
        return -1;
    }
    memcpy(pubkey->privkey_fingerprint.data, fingerprint,
        fingerprint_algo->len);
    memcpy(privkey->fingerprint.data, fingerprint, fingerprint_algo->len);

    free(fingerprint);

    if (key_algo->sign(pubkey->self_sig_data, pubkey->fingerprint.data,
        fingerprint_algo->len, privkey->data))
    {
        return -1;
    }
    return 0;
}

int pot_key_init(struct pot_pubkey * pubkey, struct pot_privkey * privkey,
    const struct pot_key_algo * key_algo,
    const struct pot_fingerprint_algo * fingerprint_algo)
{
    /* Nothing to do if no keys */
    if (pubkey == NULL && privkey == NULL)
    {
        return -1;
    }
    if (pubkey != NULL)
    {
        if (key_algo != NULL || !key_algo->id)
        {
            /* Set up key algorithm and proper amount of memory */
            pubkey->key_algo = *key_algo;
            if ((pubkey->data =
                (uint8_t *) malloc(pubkey->key_algo.public_len)) == NULL)
            {
                perror("An error occured in function pot_key_init");
                return -1;
            }
            if ((pubkey->self_sig_data =
                (uint8_t *) malloc(pubkey->key_algo.signature_len)) == NULL)
            {
                perror("An error occured in function pot_key_init");
                return -1;
            }
        }
        else
        {
            /* Otherwise this key has an undefined algo */
            pubkey->key_algo = pot_key_algo_list[0];
            pubkey->data = NULL;
            pubkey->self_sig_data = NULL;
        }
        if (fingerprint_algo != NULL || !fingerprint_algo->id)
        {
            /* Set up fingerprint algorithm and proper memory amount */
            pubkey->fingerprint.fingerprint_algo = *fingerprint_algo;
            pubkey->privkey_fingerprint.fingerprint_algo = *fingerprint_algo;
            if ((pubkey->fingerprint.data = (uint8_t *) malloc(
                pubkey->fingerprint.fingerprint_algo.len))
                == NULL)
            {
                perror("An error occured in function pot_key_init");
                return -1;
            }
            if ((pubkey->privkey_fingerprint.data = (uint8_t *) malloc(
                pubkey->privkey_fingerprint.fingerprint_algo.len)) == NULL)
            {
                perror("An error occured in function pot_key_init");
                return -1;
            }
        }
        else
        {
            /* Otherwise this fingerprint has an undefined algo */
            pubkey->fingerprint.fingerprint_algo = pot_fingerprint_algo_list[0];
            pubkey->privkey_fingerprint.fingerprint_algo =
                pot_fingerprint_algo_list[0];
            pubkey->fingerprint.data = NULL;
        }
        /* Initialize the signature linked list */
        if ((pubkey->signatures =
            malloc(sizeof(struct pot_sig_list_head))) == NULL)
        {
            perror("An error occured in function pot_key_init");
            return -1;
        }
        SLIST_INIT(pubkey->signatures);
    }
    if (privkey != NULL)
    {
        if (key_algo != NULL || !key_algo->id)
        {
            /* Set up key algorithm and proper amount of memory */
            privkey->key_algo = *key_algo;
            if ((privkey->data =
                (uint8_t *) malloc(privkey->key_algo.private_len)) == NULL)
            {
                perror("An error occured in function pot_key_init");
                return -1;
            }
        }
        else
        {
            /* Otherwise this key has an undefined algo */
            privkey->key_algo = pot_key_algo_list[0];
            privkey->data = NULL;
        }
        /* Set up fingerprint algo and memory */
        if (fingerprint_algo != NULL || !fingerprint_algo->id)
        {
            privkey->fingerprint.fingerprint_algo = *fingerprint_algo;
            privkey->pubkey_fingerprint.fingerprint_algo = *fingerprint_algo;
            if ((privkey->fingerprint.data =
                (uint8_t *) malloc(privkey->fingerprint.fingerprint_algo.len))
                == NULL)
            {
                perror("An error occured in function pot_key_init");
                return -1;
            }
            if ((privkey->pubkey_fingerprint.data =
                (uint8_t *) malloc(privkey->
                pubkey_fingerprint.fingerprint_algo.len)) == NULL)
            {
                perror("An error occured in function pot_key_init");
                return -1;
            }
        }
        else
        {
            privkey->fingerprint.fingerprint_algo =
                pot_fingerprint_algo_list[0];
            privkey->pubkey_fingerprint.fingerprint_algo =
                pot_fingerprint_algo_list[0];
            privkey->fingerprint.data = NULL;
            privkey->pubkey_fingerprint.data = NULL;
        }
    }
    return 0;
}

int pot_key_del(struct pot_pubkey * pubkey, struct pot_privkey * privkey)
{
    if (pubkey == NULL && privkey == NULL)
    {
        return -1;
    }
    if (pubkey != NULL)
    {
        free(pubkey->data);
        free(pubkey->self_sig_data);
        free(pubkey->signatures);
        free(pubkey->fingerprint.data);
        free(pubkey->privkey_fingerprint.data);
    }
    if (privkey != NULL)
    {
        free(privkey->data);
        free(privkey->fingerprint.data);
        free(privkey->pubkey_fingerprint.data);
    }
    return 0;
}
