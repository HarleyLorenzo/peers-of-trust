# Peers of Trust

An in-development peer-to-peer network library utilizing modern encryption and 
a web-of-trust infrastructure to allow verified, safe, and private p2p networks

## Compiling
You will need a compiler that supports C11, which cmake should automatically
detect and use, however you can set your own compiler through cmake arguments

### Dependencies
NOTE: This dependencies are not finalized as the intiial alpha development is 
still ongoing
`cmake >=3.1`
`libsodium >=1.0.17`

### Compilation Steps
cmake . [compilation options]
make

#### Compilation Options
At this point in development, there are additional compilation options

## License
See LICENSE

## Contributing
See CONTRIBUTING.md as well as the Code of Conduct at CODE_OF_CONDUCT.md

## Versioning
Peers of Trust uses [Semantic Versioning](https://semver.org/)

## Changelog
See CHANGELOG

## Authors/Maintainers
See AUTHORS
